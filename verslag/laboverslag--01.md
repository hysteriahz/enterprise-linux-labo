## Laboverslag: Taak 1 - Actualiteit

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. Om deze taak tot een goed einde te brengen heb ik ervoor gekozen om een eigen rol te schrijven voor vsftpd
2. één van de belangrijkste zaken om dit te verwezenlijken is beginnen met een goede directory structuur. Ik maak de mappen handlers, tasks en templates aan met daarin .yml files of een config file in het geval van templates
3. In tasks/main.yml beginnen we met het schrijven van onze playbook. In deze playbook schrijven we een taak die de nodige packages voor vsftpd installeert en de service start.
4. Verder zijn in dit bestand ook taken met de nodige regels voor de firewall en de SELinux instellingen
5. Tot slot is er ook een taak die het configuratiebestand voor vsftpd kopieert
6. In handlers/main.yml maken we 2 taken aan die instaan voor het reloaden van de ftp service en voor het herstarten van de firewall
7. Het configuratiebestand voor vsftpd halen we van de server en we vullen de variabelen in die wij nodig hebben

### Testplan en -rapport

- De gegeven BATS-testen voor vsftpd laten draaien. Deze slagen allemaal.
- Kijken als we via de browser naar ftp://172.16.0.11 kunnen surfen, dit lukt en er kan ingelogd worden. We loggen op de host in met een gebruiker en gaan naar zijn homefolder, daar maken we een testdirectory aan. Nu kijken we als we deze in onze browser tevoorschijn zien komen
- Overzicht van de shares bekijken via smbclient -L
- Wanneer we naar ftp://172.16.0.11 gaan kunnen we aan de hand van de credentials kijken als we het gewenste resultaat hebben

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Troubleshooten van de rol die ik geschreven heb ging vrij vlot. Eerst wou de service niet draaien doordat de service niet doorgelaten werd door de firewall. Verder had ik veel problemen met de lees- en schrijfrechten in de vsftpd bats file. Uiteindelijk zag ik via ls -l /srv/shares/ dat de permissies niet overeenkwamen. Ik had ook een probleem met wachtwoorden die ik niet geëncripteerd had, nadat ik dit gedaan had slaagden alle testen.

#### Wat ging niet goed?

In het begin was het moeilijk om zelf een rol te schrijven en nu juist te weten wat er allemaal nodig is om tot het gewenste resultaat te komen. Naarmate de opdracht vorderde had ik hier een beter overzicht op.

#### Wat heb je geleerd?

Gericht troubleshooten en gebruik maken van de juiste informatiebronnen

#### Waar heb je nog problemen mee?

Geen

### Referenties

docs.ansible.com
