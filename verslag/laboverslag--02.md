## Laboverslag: Taak 02 - lamp

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. Nieuwe rollen aanmaken (web & db)
2. De juiste services aanvullen
3. Mapje host_vars aanmaken met variabelen voor de VM
4. Firewallinstellingen aanpassen zodat services toegang krijgen + sellinux
5. BATS uitvoeren resulteert in 6 failures, vooral mariadb related

### Testplan en -rapport

- 

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Installatie van de rol web met httpd en wordpress

#### Wat ging niet goed?

Installatie van MariaDb geeft problemen met het root password. Overzicht bewaren van de gehele structuur van het project. Plotseling slaagt de test voor wordpress niet meer.

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

#### Waar heb je nog problemen mee?

Self-signed certificate, mariadb, wordpress (bitbucket versie zou moeten werken)

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
