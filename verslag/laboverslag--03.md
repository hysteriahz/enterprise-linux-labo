## Laboverslag: Taak 03 - DNS met BIND

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. De rol bertvv.bind uitgevoerd aan de hand van het scriptje roledeps.sh
2. De documentatie doorgenomen die bij deze rol hoort
3. Vagrant_hosts aanvullen met de nieuwe hosts en in site.yml de juiste rollen toewijzen
4. In het bestand all.yml de firewallvariabelen aanpassen zodat dns wordt toegelaten
5. In het bestand pu001.yml de nodige variabelen declareren, in pu002.yml komt dezelfde informatie al is hier de  variabele bind_zone_hosts overbodig

### Testplan en -rapport

- De bijhorende BATS testen geven geen gefaalde testen terug
- Met gebruik van het commando nslookup aantonen dat de dns aanwezig is en correct werkt

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Alles ging vrij goed volgens plan, de rol overnemen en deze aan de praat krijgen door alle nodige variabelen in te vullen en te verstaan

#### Wat ging niet goed?

De variabele bind_zone_mail_servers was niet correct ingesteld waardoor deze test maar bleef falen. Uiteindelijk lag een typo aan de basis van dit probleem. Verder had ik ook een probleem met een variabele die ik verkeerd configureerd had, die ik daarna had aangepast, maar de dns nam nog altijd deze oude foutieve waarde mee. Dit kon ik oplossen door de vagrant machine te destroyen en reloaden.

#### Wat heb je geleerd?

De werking van variabelen waren in het begin onduidelijker, maar dit begint vlotter en vlotter te lopen. 

#### Waar heb je nog problemen mee?

Ik had nog een probleem met mijn certificaten die verloren zijn gegaan, waardoor mijn pu004 niet meer alle testen slaagt. Na overleg moet ik werken met pretasks die dezen keys automatisch zouden moeten genereren.

### Referenties

bertvv.bind
