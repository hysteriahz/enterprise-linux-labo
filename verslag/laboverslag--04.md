## Laboverslag: Taak 4 - troubleshooting

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. Bottom up approach gebruiken dus kijken we eerst naar de kabels in de VM, deze geven geen probleem
2. Interface enp0s8 is down. We gaan de variabele onboot=no veranderen naar yes met het commando sudo vi/etc/sysconfig/network-scripts/enp0s8
3. Pingen naar default gateway en DNS, dit gaf geen probleem
4. Dan heb ik gekeken naar de inhoud van etc/resolv.conf, dit ziet er ok uit
5. Vervolgens een dig uitgevoerd naar google.com met succes
6. Nu is het tijd om naar de transportlaag te gaan kijken, kijken naar de service bind. Deze heb ik geïnstalleerd
7. Toegang gegeven aan 53/udp aan de firewall en enkele services toegevoegd waaronder dns en mdns
7. In named.conf was de tweede zone niet correct geconfigureerd en de optie listen-on port 53 ook niet. Veranderd naar listen-on port 53 {any; }
7. In var/named/192.168.56.in-addr.arpa zijn 12 en 13 omgewisseld, dit veranderen we terug
8. In /var/named/cynalco.com zaten er ook enkele fouten. Het ip adres van butterfree en beedle is omgekeerd. In dit bestand heb ik ook een alias aangemaakt voor mankey. Tot slot onder golbat nog een regel toegevoegd met volgende informatie MS2 IN CNAME tamatma
9. In /var/named/2.0.192.in-addr.arpa punten toegevoegd achter de domeinnamen
10. Testen runnen

### Testplan en -rapport

- Dig @192.168.56.42 tamatma.cynalco.com > dit was succesvol
- Testscript in BATS gerunt met als uitvoer 8 geslaagde testen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

Uiteindelijk de hele opdracht wanneer ik de service named aan het werken kreeg.

#### Wat ging niet goed?

Het wou maar niet lukken om de service named aan het runnen te krijgen. Uiteindelijk is dit toch gelukt door in 2.0.192.in-addr.arpa enkele punten toe te voegen die ik over het hoofd had gezien.

#### Wat heb je geleerd?

Meer letten op de kleine details in configuratiebestanden, niets is vanzelfsprekend bij een opdracht als deze. De fout zat dan ook in een bestand dat ik nog niet bekeken had.

Niet vergeten de service te restarten wanneer er aanpassingen zijn gebeurd.

#### Waar heb je nog problemen mee?

Geen

### Referenties

Slides van Mr. Van Vreckem en de pdf NetworkTroubleshooting
