## Laboverslag: Taak 5 - Een fileserver met samba en vsftpd

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. In het bestand vagrant_hosts voegen we onze nieuwe host toe namelijk pr011
2. Ook in site.yml vullen we de host in en kennen we de rollen toe die deze host zal uitvoeren.
2. Voor samba gebruik ik de rol die ons ter beschikking is gesteld. Met behulp van het script role-deps.sh installeren we deze.
3. Dan is het tijd om bij onze host_vars een nieuw bestandje aan te maken met naam van onze host. Daarin komen dan alle variabelen die nodig zijn om onze samba server te laten werken. Het is hier dat de users aangemaakt worden, de shares en de groups.
4. Niet vergeten om samba door de firewall te laten in de group_vars, die op alle hosts toegepast worden.
5. Eigen rol maken voor vsftpd, eerst en vooral zorgen we voor de gewenste directory structuur
6. In tasks/main.yml zorgen we ervoor dat vsftpd wordt geïnstalleerd en wordt doorgelaten door de firewall
7. Vervolgens halen we het configuratiebestand op dat door ansible is
8. Om anonieme gebruikers geen toegang te geven gebruiken we het replace statement die in het configuratiebestand de variabele anonymous_enable op NO zet.

### Testplan en -rapport

- Gegeven BATS testen uitvoeren, voor samba slagen deze allemaal. Bij vsftpd slagen de testen op de permissies niet want daar ben ik niet geraakt.
- Samba service benaderen via windows verkenner \\files. Als we dit doen krijg ik alle shares, en mits de juiste credentials kan er ingelogd worden op de bijhorende share
- Kijken als we via de browser naar ftp://172.16.0.11 kunnen surfen, dit lukt en er kan ingelogd worden. We loggen op de host in met een gebruiker en gaan naar zijn homefolder, daar maken we een testdirectory aan. Nu kijken we als we deze in onze browser tevoorschijn zien komen
- Overzicht van de shares bekijken via smbclient -L

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

Samba ging in het algemeen vrij goed. De users, shares & groups met juiste rechten aanmaken ging vrij vlot. Ik had enkel een probleem met een user die al eens was aangemaakt, dit gaf een conflict. De rol uitschrijven was in het begin moeilijk maar naar het einde toe ging dit toch al iets beter.

#### Wat ging niet goed?

Zelf een rol uitschrijven vond ik persoonlijk iets moeilijker. Zaken die normaal direct zouden moeten werken (omdat ze niet zo moeilijk lijken) lukten bij mij niet direct. De service starten gaf veel problemen, maar het is uiteindelijk goed gekomen al weet ik niet waar het probleem lag.

#### Wat heb je geleerd?

Ik heb geleerd om juiste documentatie op te zoeken, er is zoveel voor handen dat je soms teveel wilt doen en dingen begint toe te passen die eigenlijk helemaal niet nodig zijn. Zaken die je niet begrijpt blijf je beter van of zoek je op. Ik heb geleerd om zelf, ook al is het maar een basisrol, te schrijven.

#### Waar heb je nog problemen mee?

De share permissies zijn nog niet in orde voor vsftpd

### Referenties

docs.ansible.com
