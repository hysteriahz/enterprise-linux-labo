## Laboverslag: Taak 6 - DHCP

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. We maken ook hier gebruik van een rol die ons ter beschikking staat namelijk bertvv.dhcp
2. We maken in site.yml een nieuwe host pr001 aan en kennen de rol toe aan deze host. Aan de hand van het script role-deps.sh halen we deze rol binnen en kunnen we er met beginnen werken.
3. Vergeet ook niet om in vagrant_hosts.yml de dhcp server toe te voegen
4. We zorgen er ook voor dat de firewall dhcp toelaat in het bestand all.yml in de group vars. Daarvoor geven we  port 67 en 68 udp toegang en laten we de service dhcp door.
4. Daarna maken we in onze host_vars een bestandje pr001.yml aan en vullen we daar alle variabelen in die we nodig hebben om onze dhcp server te configureren
5. We beginnen met de subnets te declareren en vervolgens declaren we ook de DNS-servers, het global domain en de dhcp pools aan de hand van de variabelen die voor handen zijn

### Testplan en -rapport

- Maak (manueel) een nieuwe VirtualBox VM met twee host-only netwerkinterfaces die beide aangesloten zijn op het
netwerk met IP 172.16.0.0/16.
2. Noteer van de ene interface het MAC-adres en definieer een reservatie voor dit adres
3. Boot de VM met een live-cd, zet zo nodig de interfaces aan en zorg dat ze een IP opvragen via DHCP
4. Controleer dat de ene interface het gereserveerde IP-adres krijgt en de andere één uit de pool


### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Opzetten van de rol aan de hand van bertvv.dhcp en declaratie van de variabelen

#### Wat ging niet goed?

Het testen van deze server vond ik het moeilijkste van deze opdracht. Ook de vele variabelen maakten het ingewikkelder dan ik eerst had gedacht.

#### Wat heb je geleerd?

Ik heb geleerd om stap voor stap te werken. Na al de andere opdrachten is het eenvoudig geworden om vanuit een bestaande rol verder te werken.

#### Waar heb je nog problemen mee?

Het testen van de server

### Referenties

https://galaxy.ansible.com/detail#/role/4859

https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/
