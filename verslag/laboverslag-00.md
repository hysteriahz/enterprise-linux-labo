## Laboverslag: Taak 0 - opzetten werkomgeving

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. Nieuwe private repository aangemaakt op bitbucket. (Benodigde software heb ik nog van vorig jaar)
2. Genereren ssh sleutelpaar en configureren in bitbucket en lokaal
3. Fork van de gegeven repository
4. Lokale kopie van de repository aanmaken met git clone
5. Het scriptje role-deps.sh uitvoeren op de plaats van je lokale repository
6. De bertvv.el7 role aanvullen in het bestand site.yml
7. Aanvullen group-vars namelijk het bestand all.yml, daar komen alle nodige variabelen in die op alle hosts moeten worden toegepast

### Testplan en -rapport

0. Ga op het hostsysteem naar de directory met de lokale kopie van de repository.
1. Voer vagrant status uit. We zien nu één VM met naam pu004 die de status "not created" heeft
2. Voer vagrant up pu004 uit. Het commando slaagt zonder fouten.
3. Log in op de server met vagrant ssh srv010 en voer de acceptatietests uit. Deze slagen allemaal.
4. Log uit en log vanop het hostsysteem opnieuw in, maar nu met ssh. Er mag geen wachtwoord gevraagd worden. Dit lukt op dit moment niet.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Initialisatie van het project
- Installeren en gebruik maken van de roles
- Aanpassen van de group-vars
- Testen

#### Wat ging niet goed?

Weinig tot geen problemen gehad enkel met het ssh sleutelpaar tot de repository. Bij een git push is er nog altijd een wachtwoord nodig.

#### Wat heb je geleerd?

De optie core.autocrlf=input bij een git clone is nodig om adaptaties van witruimte in configuratiebestanden te voorkomen van linux naar windows

#### Waar heb je nog problemen mee?

ssh sleutelpaar bij git push

### Referenties

/
