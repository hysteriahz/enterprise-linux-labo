## Laboverslag: Taak 7 - troubleshooting 2

- Naam cursist: Martijn Toch
- Bitbucket repo: https://bitbucket.org/hysteriahz/enterprise-linux-labo

### Procedures

1. Eerst en vooral laden we de juiste keys in met sudo loadkeys be-latin1
2. Daarna heb ik de bats test uitgevoerd en gekeken in het bats bestand wat er juist getest wordt
3. Om de eerste test, NetBIOS name resolution should work, te doen slagen heb ik de nmb service laten starten met sudo service nmb start
4. Daarna het commando sudo setsebool -P samba_export_all_rw uitgevoerd
5. Om de permissies van de shares in te stellen heb ik gebruik gemaakt van sudo chown user share om de owner in te stellen. En vervolgens sudo chmod -R xxx om de juiste access te voorzien
6. Daarna heb ik de smb.Conf bekeken en heb ik voor sommige shares nog enkele zaken toegevoegd, bijvoorbeeld bij foxtrot moest er nog een write_list = @foxtrot toegevoegd worden
7. Vervolgens heb ik samba toegang gegeven tot de firewall met het commando sudo firewall-cmd --permanent --add-service=samba
8. Getest als samba toegankelijk is via files
8. Laboverslag maken


### Testplan en -rapport

- In de directory /srv/shares/ gebruik maken van ls -a -l om het overzicht te bekijken
- Bats testen laten lopen, deze slagen allemaal
- Via files - connect to server kijken als we kunnen connecteren met server, zowel met het IP adres als met de naam brokensamba, ook dit lukt

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Dit labo ging vrij vlot eenmaal we op gang waren. Alles was relatief snel in orde.

#### Wat ging niet goed?

Permissies heb ik toch moeten opfrissen tijdens het troubleshooten, sommige zaken lukten eerst niet door verkeerde argumenten na chmod -R

#### Wat heb je geleerd?

Rustig blijven en doorwerken

#### Waar heb je nog problemen mee?

Geen

### Referenties

man page chmod en chown
